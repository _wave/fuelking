package de.andreerk.android.fuelking.core.persistence.impl;

import android.content.Context;

import java.util.Collection;

import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.core.model.StatisticsEntry;
import de.andreerk.android.fuelking.core.persistence.IPersistence;

/**
 * Created by Andre Erk on 14.06.2016.
 */
public class Persistence implements IPersistence {

    private StationsDataSource sds;

    private static Persistence instance = null;

    public Persistence(Context context) {
        this.sds = new StationsDataSource(context);
    }

    public static Persistence getInstance(Context context) {
        if(instance == null) {
            instance = new Persistence(context);
        }
        return instance;
    }

    public void createDb () {

    }

    @Override
    public boolean createStations(Collection<Station> stations) {
        return sds.createStations(stations);
    }

    @Override
    public void deleteAllStations() {
        sds.deleteAllStations();
    }

    @Override
    public void deleteStation(String id) {
        sds.deleteStation(id);
    }

    @Override
    public void deleteStation(Station station) {
        sds.deleteStation(station);
    }

    @Override
    public Collection<Station> getAllStations() {
        return sds.getAllStations();
    }

    @Override
    public Collection<StatisticsEntry> getAvgValues() {
        return sds.getAvgValues();
    }

    @Override
    public Collection<StatisticsEntry> getAvgOfSingleStation(String id) {
        return sds.getAvgOfSingleStation(id);
    }
}
