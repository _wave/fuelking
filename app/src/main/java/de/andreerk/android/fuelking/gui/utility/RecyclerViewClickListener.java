package de.andreerk.android.fuelking.gui.utility;

import android.view.View;

/**
 * Created by Andre Erk on 17.06.2016.
 */
public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v,int position);
}
