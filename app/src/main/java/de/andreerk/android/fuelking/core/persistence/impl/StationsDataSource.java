package de.andreerk.android.fuelking.core.persistence.impl;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.core.model.StatisticsEntry;
import de.andreerk.android.fuelking.core.persistence.IPersistence;

/**
 * Created by Andre Erk on 14.06.2016.
 */
public class StationsDataSource implements IPersistence {
    // Datenbank Felder
    private volatile SQLiteDatabase database;
    private volatile MySQLiteHelper dbHelper;
    private String[] allColumns = {
            MySQLiteHelper.COLUMN_UID,
            MySQLiteHelper.COLUMN_NAME,
            MySQLiteHelper.COLUMN_LAT,
            MySQLiteHelper.COLUMN_LNG,
            MySQLiteHelper.COLUMN_BRAND,
            MySQLiteHelper.COLUMN_DIST,
            MySQLiteHelper.COLUMN_DIESEL,
            MySQLiteHelper.COLUMN_E5,
            MySQLiteHelper.COLUMN_E10,
            MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_STREET,
            MySQLiteHelper.COLUMN_HOUSENUMBER,
            MySQLiteHelper.COLUMN_POSTCODE,
            MySQLiteHelper.COLUMN_PLACE,
            MySQLiteHelper.COLUMN_ISOPEN,
            MySQLiteHelper.COLUMN_TIMESTAMP,
            MySQLiteHelper.COLUMN_HOUR };

    public StationsDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    @Override
    public synchronized boolean createStations(Collection<Station> stations) {
        open();
        String sql = "INSERT INTO " + MySQLiteHelper.TABLE_STATION +" ("
                + MySQLiteHelper.COLUMN_NAME + ","
                + MySQLiteHelper.COLUMN_LAT + ","
                + MySQLiteHelper.COLUMN_LNG + ","
                + MySQLiteHelper.COLUMN_BRAND + ","
                + MySQLiteHelper.COLUMN_DIST + ","
                + MySQLiteHelper.COLUMN_DIESEL + ","
                + MySQLiteHelper.COLUMN_E5 + ","
                + MySQLiteHelper.COLUMN_E10 + ","
                + MySQLiteHelper.COLUMN_ID + ","
                + MySQLiteHelper.COLUMN_STREET + ","
                + MySQLiteHelper.COLUMN_HOUSENUMBER + ","
                + MySQLiteHelper.COLUMN_POSTCODE + ","
                + MySQLiteHelper.COLUMN_PLACE + ","
                + MySQLiteHelper.COLUMN_ISOPEN + ","
                + MySQLiteHelper.COLUMN_TIMESTAMP + ", "
                + MySQLiteHelper.COLUMN_HOUR
                + ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        database.beginTransaction();
        SQLiteStatement stmt = database.compileStatement(sql);

        for (Station station : stations) {
            stmt.bindString(1, station.getName());
            stmt.bindDouble(2, station.getLat());
            stmt.bindDouble(3, station.getLng());
            stmt.bindString(4, station.getBrand() == null ? "0" : station.getBrand());
            stmt.bindDouble(5, station.getDist());
            stmt.bindDouble(6, station.getDiesel());
            stmt.bindDouble(7, station.getE5());
            stmt.bindDouble(8, station.getE10());
            stmt.bindString(9, station.getId());
            stmt.bindString(10, station.getStreet());
            stmt.bindString(11, station.getHouseNumber() == null ? "0" : station.getHouseNumber());
            stmt.bindLong(12, station.getPostCode());
            stmt.bindString(13, station.getPlace());
            stmt.bindLong(14, station.isOpen() ? 1 : 0);
            stmt.bindLong(15, station.getTimestamp().getTime());
            stmt.bindLong(16, station.getHour());

            long insertId = stmt.executeInsert();
            stmt.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();

        close();
        return true;
    }

    @Override
    public void deleteStation(String id) {
        open();
        database.delete(MySQLiteHelper.TABLE_STATION, MySQLiteHelper.COLUMN_ID
                + " = '" + id + "'", null);
        close();
    }

    public synchronized void deleteStation(Station station) {
        open();
        int uid = station.getUid();
        database.delete(MySQLiteHelper.TABLE_STATION, MySQLiteHelper.COLUMN_UID
                + " = " + uid, null);
        close();
    }

    public synchronized void deleteAllStations () {
        open();
        database.delete(MySQLiteHelper.TABLE_STATION, MySQLiteHelper.COLUMN_UID
                + " != -1", null);
        close();
    }

    public synchronized Collection<Station> getAllStations() {
        open();
        Collection<Station> stations;
        synchronized (this.database) {
            stations = new ArrayList<Station>();

            Cursor cursor = database.query(MySQLiteHelper.TABLE_STATION,
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Station station = cursorToStation(cursor);
                stations.add(station);
                cursor.moveToNext();
            }
            cursor.close();
            close();
        }

        return stations;
    }

    public synchronized Collection<StatisticsEntry> getAvgOfSingleStation (String id) {
        open();
        Collection<StatisticsEntry> ses = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT hour, AVG(e5), AVG(e10), AVG(diesel) FROM station WHERE id = ? GROUP BY hour", new String[]{id});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            StatisticsEntry se = cursorToStatisticsEntry(cursor);
            ses.add(se);
            cursor.moveToNext();
        }
        cursor.close();
        close();

        return ses;
    }

    public synchronized Collection<StatisticsEntry> getAvgValues () {
        open();
        Collection<StatisticsEntry> ses = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT hour, AVG(e5), AVG(e10), AVG(diesel) FROM station GROUP BY hour", new String[0]);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            StatisticsEntry se = cursorToStatisticsEntry(cursor);
            ses.add(se);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return ses;
    }

    /**
     * Wandelt Cursor in StatistiscEntry um
     * @param cursor
     * @return StatisticsEntry
     */
    private synchronized StatisticsEntry cursorToStatisticsEntry (Cursor cursor) {
        StatisticsEntry se = new StatisticsEntry();
        se.setHour(cursor.getInt(0));
        se.setAvgE5(cursor.getDouble(1));
        se.setAvgE10(cursor.getDouble(2));
        se.setAvgDiesel(cursor.getDouble(3));
        return se;
    }

    /**
     * Wandelt Cursor in Station um
     * @param cursor
     * @return Station
     */
    private synchronized Station cursorToStation(Cursor cursor) {
        Station station = new Station();
        station.setUid(cursor.getInt(0));
        station.setName(cursor.getString(1));
        station.setLat(cursor.getDouble(2));
        station.setLng(cursor.getDouble(3));
        station.setBrand(cursor.getString(4));
        station.setDist(cursor.getDouble(5));
        station.setDiesel(cursor.getDouble(6));
        station.setE5(cursor.getDouble(7));
        station.setE10(cursor.getDouble(8));
        station.setId(cursor.getString(9));
        station.setStreet(cursor.getString(10));
        station.setHouseNumber(cursor.getString(11));
        station.setPostCode(cursor.getInt(12));
        station.setPlace(cursor.getString(13));
        station.setOpen(cursor.getShort(14) > 0);
        station.setTimestamp(new Timestamp(cursor.getLong(15)));
        station.setHour(cursor.getInt(16));
        return station;
    }
}
