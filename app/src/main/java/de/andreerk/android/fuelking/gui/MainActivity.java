package de.andreerk.android.fuelking.gui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import de.andreerk.android.fuelking.R;
import de.andreerk.android.fuelking.core.StationManager;
import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.core.persistence.impl.AndroidDatabaseManager;

public class MainActivity extends AppCompatActivity implements StationListFragment.OnListFragmentInteractionListener, StartFragment.OnFragmentInteractionListener,
        StationMapFragment.OnFragmentInteractionListener{


    private ViewPager mViewPager;
    public  static FragmentManager mFragmentManager;

    private StationManager mStationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        if (mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(mViewPager);
        }
        mFragmentManager = getSupportFragmentManager();

        // Nach GPS-Permision fragen
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, StationManager.PERMISSION_ACCESS_FINE_LOCATION );
        }

        // StationManager-Instanz holen
        mStationManager = StationManager.getInstance(getApplicationContext());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.credits) {
            Intent credits = new Intent(this, CreditsActivity.class);
            startActivity(credits);
            return true;
        } else if (id == R.id.dbmanager) {
            Intent dbmanager = new Intent(this, AndroidDatabaseManager.class);
            startActivity(dbmanager);
        } else if (id == R.id.statistics) {
            Intent statistics = new Intent(this, StatisticsActivity.class);
            startActivity(statistics);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(Station station) { }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    /**
     * Methode zum Geoposition finden in StationManager setzen
     * @param view
     */
    public void onRadioButtonClicked (View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.cbRememberInput);
        if (cb != null) {
            switch (view.getId()) {
                case R.id.rbGPS:
                    mStationManager.setmSearchMethod("GPS");
                    cb.setChecked(false);
                    break;

                case R.id.rbZIP:
                    mStationManager.setmSearchMethod("ZIP");
                    cb.setChecked(false);
                    break;
            }
        }
    }

    /**
     * Postleitzahl in StationManager setzen
     * @param view
     */
    public void sendZipToControl (View view) {
        EditText mEtZip = (EditText) findViewById(R.id.etZip);
        mStationManager.setmZip(mEtZip != null ? mEtZip.getText().toString() : null);

        // Softkeyboard schliessen
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Tankstellen auf Befehl durch Button suchen und dann zeitverzoegert in den Tankstellen-Tab wechseln
     * @param view
     */
    public void searchStations (View view) {
        Button btn = (Button) findViewById(R.id.btnSearchStations);
        if (btn != null) {
            btn.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mViewPager.setCurrentItem(1);
                }
            }, 1500);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                mStationManager.loadStations();
            }
        }).start();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            switch (position) {
                case 0:
                    return StartFragment.newInstance();
                case 1:
                    return StationListFragment.newInstance(1);
                case 2:
                    return StationMapFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Start";
                case 1:
                    return "Tankstellen";
                case 2:
                    return "Karte";
            }
            return null;
        }
    }
}
