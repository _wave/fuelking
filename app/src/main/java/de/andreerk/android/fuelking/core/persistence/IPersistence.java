package de.andreerk.android.fuelking.core.persistence;

import java.util.Collection;

import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.core.model.StatisticsEntry;

/**
 * Created by Andre Erk on 30.06.2016.
 * Interface um auf Daten (aus der Datenbanke i.d.F.) zuzugreifen
 */
public interface IPersistence {
    /**
     * Erstellt fur jede Tankstelle einen Eintrag in der Datenbank
     * @param stations Liste von Tankstellen von der API
     * @return Miss-/Erfolg
     */
    boolean createStations(Collection<Station> stations);

    /**
     * Loescht den gesamten Inhalt der Station Tabelle
     */
    void deleteAllStations();

    /**
     * Loescht alle Eintraege mit der entsprechenden ID in der Station Tabelle
     * @param id
     */
    void deleteStation(String id);

    /**
     * Loescht den Eintrag mit der entsprechenden UID der Tankstelle
     * @param station
     */
    void deleteStation(Station station);

    /**
     * Holt alle Eintraege aus der Station Tabelle
     * @return
     */
    Collection<Station> getAllStations();

    /**
     * Holt die Durchschnittswerte von allen Tankstellen
     * @return
     */
    Collection<StatisticsEntry> getAvgValues();

    /**
     * Holt die Durchschnittswerte von einer bestimmten Tankstelle mit der gegebenen ID
     * @param id ID der Tankstelle
     * @return
     */
    Collection<StatisticsEntry> getAvgOfSingleStation(String id);
}
