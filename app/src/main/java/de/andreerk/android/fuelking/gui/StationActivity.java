package de.andreerk.android.fuelking.gui;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.andreerk.android.fuelking.R;
import de.andreerk.android.fuelking.core.StationManager;
import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.core.model.StatisticsEntry;

public class StationActivity extends AppCompatActivity {

    private Station mStation;
    private StationManager mStationManager;

    private LineChart mChart;
    private ArrayList<StatisticsEntry> mSes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_station);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Uebermittelte Tankstelle holen
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mStation = new Gson().fromJson(extras.getString("STATION"), Station.class);
        }

        // Activity Titel setzen
        setTitle(this.mStation.getName());


        new Thread(new Runnable() {
            @Override
            public void run() {
                // StationManager-Instanz holen
                mStationManager = StationManager.getInstance(getApplicationContext());
                // Durchschnitte holen
                mSes = (ArrayList<StatisticsEntry>) mStationManager.getSingleStationAvg(mStation.getId());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Chart Einstellungen setzen
                        mChart = (LineChart) findViewById(R.id.chart);
                        mChart.setDescription("Durchschnittspreise");
                        mChart.setNoDataTextDescription("Noch keine Daten verfügbar.");
                        mChart.setNoDataText("Noch keine Daten verfügbar.");
                        mChart.setBackgroundColor(Color.parseColor("#4CAF50"));
                        mChart.setDrawGridBackground(false);
                        mChart.setAutoScaleMinMaxEnabled(true);

                        mChart.getXAxis().setDrawGridLines(false);
                        mChart.getXAxis().setEnabled(true);
                        mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                        mChart.getXAxis().setTextColor(Color.WHITE);
                        mChart.getXAxis().setAxisLineColor(Color.WHITE);

                        mChart.getAxisLeft().setDrawGridLines(false);
                        mChart.getAxisLeft().setTextColor(Color.WHITE);
                        mChart.getAxisLeft().setAxisLineColor(Color.WHITE);
                        mChart.getAxisLeft().setEnabled(false);

                        mChart.getAxisRight().setDrawGridLines(false);
                        mChart.getAxisRight().setTextColor(Color.WHITE);
                        mChart.getAxisRight().setAxisLineColor(Color.WHITE);

                        mChart.getLegend().setEnabled(true);
                        mChart.animateXY(2000,3000);

                        // X-Achse Vals setzen
                        ArrayList<String> xVals = new ArrayList<>();
                        for (int i = 0; i < 24; i++) {
                            xVals.add((i) + "");
                        }

                        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

                        // Y-Achse Vals fuer E5 setzen
                        ArrayList<Entry> e5Entries = new ArrayList<>();
                        for (int i = 0; i < mSes.size(); i++) {
                            e5Entries.add(new Entry(Float.parseFloat(String.valueOf(mSes.get(i).getAvgE5())), mSes.get(i).getHour()));
                        }
                        LineDataSet lds = new LineDataSet(e5Entries, "Super E5");
                        lds.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
                        lds.setValueTextSize(9f);
                        dataSets.add(lds);

                        // Y-Achse Vals fuer E10 setzen
                        ArrayList<Entry> e10Entries = new ArrayList<>();
                        for (int i = 0; i < mSes.size(); i++) {
                            e10Entries.add(new Entry(Float.parseFloat(String.valueOf(mSes.get(i).getAvgE10())), mSes.get(i).getHour()));
                        }
                        LineDataSet ldse10 = new LineDataSet(e10Entries, "Super E10");
                        ldse10.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
                        ldse10.setValueTextSize(9f);
                        ldse10.setColor(Color.parseColor("#212121"));
                        ldse10.setCircleColor(Color.parseColor("#212121"));
                        ldse10.setCircleColorHole(Color.parseColor("#212121"));
                        dataSets.add(ldse10);

                        // Y-Achse Vals fuer Diesel setzen
                        ArrayList<Entry> dieselEntries = new ArrayList<>();
                        for (int i = 0; i < mSes.size(); i++) {
                            dieselEntries.add(new Entry(Float.parseFloat(String.valueOf(mSes.get(i).getAvgDiesel())), mSes.get(i).getHour()));
                        }
                        LineDataSet ldsDiesel = new LineDataSet(dieselEntries, "Diesel");
                        ldsDiesel.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
                        ldsDiesel.setValueTextSize(9f);
                        ldsDiesel.setColor(Color.parseColor("#FFEB3B"));
                        ldsDiesel.setCircleColor(Color.parseColor("#FFEB3B"));
                        ldsDiesel.setCircleColorHole(Color.parseColor("#FFEB3B"));
                        dataSets.add(ldsDiesel);

                        // Alles zusammenfuehren
                        LineData ld = new LineData(xVals, dataSets);
                        mChart.setData(ld);
                        mChart.invalidate();
                    }
                });
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_statistics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.assignment) {
            // Listen sortieren und jeweiligen Minimalwert herausfinden
            StatisticsEntry minE5 = Collections.min(mSes, new Comparator<StatisticsEntry>() {
                @Override
                public int compare(StatisticsEntry lhs, StatisticsEntry rhs) {
                    return lhs.getAvgE5() < rhs.getAvgE5() ? -1 : 1;
                }
            });

            StatisticsEntry minE10 = Collections.min(mSes, new Comparator<StatisticsEntry>() {
                @Override
                public int compare(StatisticsEntry lhs, StatisticsEntry rhs) {
                    return lhs.getAvgE10() < rhs.getAvgE10() ? -1 : 1;
                }
            });

            StatisticsEntry minDiesel = Collections.min(mSes, new Comparator<StatisticsEntry>() {
                @Override
                public int compare(StatisticsEntry lhs, StatisticsEntry rhs) {
                    return lhs.getAvgDiesel() < rhs.getAvgDiesel() ? -1 : 1;
                }
            });

            // Tankzeiten oeffnen
            AlertDialog.Builder bass = new AlertDialog.Builder(this);
            bass.setTitle("Beste Tankzeit");
            bass.setItems(new String[]{"Super E5:\t\t"+minE5.getHour()+" Uhr ("+minE5.getAvgE5()+")",
                    "Super E10:\t"+minE10.getHour()+" Uhr ("+minE10.getAvgE10()+")",
                    "Diesel:\t\t\t\t\t"+minDiesel.getHour()+" Uhr ("+minDiesel.getAvgDiesel()+")"}, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) { }
            });
            AlertDialog alass = bass.create();
            alass.show();


            return true;
        } else if (id == R.id.delete) {
            // Datenbankeintraege entfernen
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Bestätigen");
            builder.setMessage("Möchten Sie die Spritpreisaufzeichnungen wirklich löschen?");

            builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    mStationManager.deleteStation(mStation.getId());
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Nichts machen
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        return super.onOptionsItemSelected(item);
    }
}
