package de.andreerk.android.fuelking.core.persistence.impl;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Andre Erk on 14.06.2016.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    // Tabellen-, Datenbank-und Spaltennamen, Datenbank-Version
    public static final String TABLE_STATION = "station";
    public static final String COLUMN_UID = "uid";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LAT = "lat";
    public static final String COLUMN_LNG = "lng";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_DIST = "dist";
    public static final String COLUMN_DIESEL = "diesel";
    public static final String COLUMN_E5 = "e5";
    public static final String COLUMN_E10 = "e10";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_STREET = "street";
    public static final String COLUMN_HOUSENUMBER = "houseNumber";
    public static final String COLUMN_POSTCODE = "postCode";
    public static final String COLUMN_PLACE = "place";
    public static final String COLUMN_ISOPEN = "isOpen";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_HOUR = "hour";

    private static final String DATABASE_NAME = "stations.db";
    private static final int DATABASE_VERSION = 8;

    // Statement zum Datenbank erstellen
    private static final String DATABASE_CREATE = "create table "
            + TABLE_STATION + "("
            + COLUMN_UID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text, "
            + COLUMN_LAT + " real, "
            + COLUMN_LNG + " real, "
            + COLUMN_BRAND + " text, "
            + COLUMN_DIST + " real, "
            + COLUMN_DIESEL + " real, "
            + COLUMN_E5 + " real, "
            + COLUMN_E10 + " real, "
            + COLUMN_ID + " text, "
            + COLUMN_STREET + " text, "
            + COLUMN_HOUSENUMBER + " text, "
            + COLUMN_POSTCODE + " integer, "
            + COLUMN_PLACE + " text, "
            + COLUMN_ISOPEN + " boolean, "
            + COLUMN_TIMESTAMP + " integer, "
            + COLUMN_HOUR + " integer "
            + ");";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATION);
        onCreate(db);
    }

    // Helper class for DB Manager GEHOERT ZU AndroidDatabaseManager
    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }
}
