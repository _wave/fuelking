package de.andreerk.android.fuelking.core.model;

import java.util.Collection;

/**
 * Created by Andre Erk on 12.06.2016.
 * Container um JSON/Gson Objekt zu mappen
 */
public class StationContainer {

    private String license;
    private String data;
    private Collection<Station> stations;


    public StationContainer() { }


    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Collection<Station> getStations() {
        return stations;
    }

    public void setStations(Collection<Station> stations) {
        this.stations = stations;
    }
}
