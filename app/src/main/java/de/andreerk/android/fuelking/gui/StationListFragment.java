package de.andreerk.android.fuelking.gui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import de.andreerk.android.fuelking.R;
import de.andreerk.android.fuelking.core.StationManager;
import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.gui.utility.DividerItemDecoration;
import de.andreerk.android.fuelking.gui.utility.MyStationRecyclerViewAdapter;
import de.andreerk.android.fuelking.gui.utility.RecyclerViewClickListener;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class StationListFragment extends Fragment implements RecyclerViewClickListener {

    private StationManager mStationManager;
    private MyStationRecyclerViewAdapter mStationRecyclerViewAdapter;
    private RecyclerViewClickListener mItemClickListener;

    private OnListFragmentInteractionListener mListener;

    private Handler handler;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StationListFragment() {
    }

    public static StationListFragment newInstance(int columnCount) {
        return new StationListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_station_list_list, container, false);

        // Adapter setzen
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            // StationManager-Instanz holen
            mStationManager = StationManager.getInstance(getActivity().getApplicationContext());

            // Aktion bei Klick auf einen Listeneintrag
            mItemClickListener = new RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, final int position) {

                    // Alert Dialog - Waehlen zwischen Navigation und Spritpreisverlauf
                    AlertDialog.Builder bass = new AlertDialog.Builder(getContext());
                    bass.setTitle("Bitte wählen");
                    bass.setItems(new String[]{"Navigation",
                            "Spritpreisverlauf"}, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    // Google Maps mit Route oeffnen
                                    Station s = mStationRecyclerViewAdapter.getmStations().get(position);
                                    double latitude = s.getLat();
                                    double longitude = s.getLng();
                                    String label = s.getName();
                                    String uriBegin = "geo:" + latitude + "," + longitude;
                                    String query = latitude + "," + longitude + "(" + label + ")";
                                    String encodedQuery = Uri.encode(query);
                                    String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                                    String url = "http://maps.google.com/maps?daddr=" + latitude + "," + longitude + "&mode=driving";
                                    Uri uri = Uri.parse(url);
                                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                    break;

                                case 1:
                                    // StationActivity mit Spritpreisverlauf oeffnen
                                    Station station = mStationRecyclerViewAdapter.getmStations().get(position);
                                    Intent intDetails = new Intent(getContext(), StationActivity.class);
                                    intDetails.putExtra("STATION", new Gson().toJson(station));
                                    startActivity(intDetails);
                                    break;
                            }
                        }
                    });
                    AlertDialog alass = bass.create();
                    alass.show();
                }
            };

            // Horizontale Linien zwischen den ListItems
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            recyclerView.setAdapter(mStationRecyclerViewAdapter = new MyStationRecyclerViewAdapter( mListener, mItemClickListener));

            handler = new Handler();
            runnable.run();

            //mStationRecyclerViewAdapter.setmStations();
        }
        return view;
    }

    /**
     * Runnable, das die Liste relativ regelmaessig aktualisiert, damit Aenderungen sichtbar werden
     */
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                mStationManager.sortStations();
                mStationRecyclerViewAdapter.setmStations(mStationManager.getmStations());
            } catch (Exception e) {
                e.printStackTrace();
            }
            handler.postDelayed(runnable, 5000);
        }
    };

    @Override
    public void setUserVisibleHint (boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mStationManager.sortStations();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mStationRecyclerViewAdapter.setmStations(mStationManager.getmStations());
                }
            });

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Station station);
    }
}
