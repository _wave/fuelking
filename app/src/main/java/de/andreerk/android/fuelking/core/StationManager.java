package de.andreerk.android.fuelking.core;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.core.model.StationContainer;
import de.andreerk.android.fuelking.core.model.StatisticsEntry;
import de.andreerk.android.fuelking.core.persistence.IPersistence;
import de.andreerk.android.fuelking.core.persistence.impl.Persistence;
import de.andreerk.android.fuelking.core.utility.RestAsyncTask;

/**
 * Created by Andre Erk on 16.06.2016.
 */
public class StationManager {

    private static StationManager instance = null;

    private IPersistence persistence;

    private Collection<Station> mStations;
    private int mRadius;
    private String mSort;
    private String mSearchMethod;
    private String mZip;

    private String mLastQuery;

    private Context mContext;
    private Handler mHandler;

    private Location mLocation;

    public static int PERMISSION_ACCESS_FINE_LOCATION;


    // Singleton
    public static StationManager getInstance(Context context) {
        if (instance == null) {
            instance = new StationManager(context);
        }
        return instance;
    }

    protected StationManager() { }

    protected StationManager(Context context) {
        this.persistence = new Persistence(context);
        this.mContext = context;
        this.mRadius = 25;
        this.mStations = new ArrayList<>();

        mHandler = new Handler();
        runnable.run();
    }

    /**
     * Runnable, das alle fuenf Minuten neue Daten herunterlaedt
     */
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                loadStations();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Auf jeden Fall alle 5 Minuten aktualisieren 300.000ms
                mHandler.postDelayed(runnable, 300000);
            }
        }
    };

    /**
     * Smartphone GPS Location oder ZIP GPS Location wird ermittelt.
     * @return
     */
    public Location getDeviceLocation() {
        switch (this.mSearchMethod) {
            case "":
            case "GPS":
                LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
                // Permission eigentlich schon in Activity abgefragt
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Toast toast = Toast.makeText(mContext, "No GPS Permission", Toast.LENGTH_SHORT);
                    toast.show();
                }
                return lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            case "ZIP": // PLZ wird eingegeben und Koordinaten werden geladen
                Geocoder geo = new Geocoder(mContext, Locale.getDefault());
                try {
                    List<Address> addresses = geo.getFromLocationName(mZip, 1);
                    Location loc = new Location("");
                    loc.setLatitude(addresses.get(0).getLatitude());
                    loc.setLongitude(addresses.get(0).getLongitude());
                    return loc;
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    /**
     * Stations/Tankstellen werden asynchron heruntergeladen und in Java Beans umgewandelt, die anschliessend gespeichert werden
     * Es wird ein Zeitstempel und die aktuelle Zeitstunde hinzugefuegt
     */
    public void loadStations() {
        final Collection<Station> stations = new ArrayList<>();
        final String[] aResult = {null};
        RestAsyncTask restAsyncTask = new RestAsyncTask() {
            @Override
            protected void onPostExecute(String result) {
                // Datum/Stunde und Zeitstempel erstellen
                Date date = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                Timestamp timestamp = new Timestamp(date.getTime());
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                aResult[0] = result;
                StationContainer stationContainer = new Gson().fromJson(aResult[0], StationContainer.class);
                // Datum und Zeitstempel hinzufuegen, Objekt in Log anzeigen
                for (Station station: stationContainer.getStations()) {
                    station.setTimestamp(timestamp);
                    station.setHour(hour);
                    Log.e("FETCHED DATA OBJECT", station.toString());
                    stations.add(station);
                }
                mStations = stations;

                // Tankstellen asynchron in Datenbank speichern
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        persistence.createStations(stations);
                    }
                }).start();

            }
        };

        // Geoposition wird ermittelt
        Location loc = getDeviceLocation();

        // API-String wird zusammengesetzt
        String base = "https://creativecommons.tankerkoenig.de/json/list.php?";
        String lat = "lat=" + loc.getLatitude();
        String lng = "&lng=" + loc.getLongitude();
        String rad = "&rad=" + mRadius;
        // Key für den Zugriff auf die freie Tankerkönig-Spritpreis-API
        // Für eigenen Key bitte hier https://creativecommons.tankerkoenig.de
        // registrieren.
        String rest = "&type=all&apikey=8c02c0d4-1183-e2e6-4ca7-47f3ddd41f22";
        String comp =  base + lat + lng + rad + rest;

        // Wenn die gleiche Query ausgefuehrt werden wuerde, wird dies nicht getan, um die API zu entlasten
        // Daher wird die API effektiv auch nicht alle fuenf Minuten belastet sofern man sich nicht schnell bewegt oder
        // wirklich GPS nutzt und nicht wie vorgegeben eine Postleitzahl
        if (!comp.equals(mLastQuery)) {
            mLastQuery =  base + lat + lng + rad + rest;
            restAsyncTask.execute(new String[] { base + lat + lng + rad + rest });
        }
    }

    /**
     * Holt die Durchschnittswerte von einer bestimmten Tankstelle mit der gegebenen ID
     * @param id ID der Tankstelle
     * @return
     */
    public Collection<StatisticsEntry> getSingleStationAvg(String id) {
        return persistence.getAvgOfSingleStation(id);
    }

    /**
     * Holt die Durchschnittswerte von allen Tankstellen
     * @return
     */
    public Collection<StatisticsEntry> getStatisticsEntries () {
        return persistence.getAvgValues();
    }

    /**
     * Loescht den gesamten Inhalt der Station Tabelle
     */
    public void deleteStations () {
        persistence.deleteAllStations();
    }

    /**
     * Loescht alle Eintraege mit der entsprechenden ID in der Station Tabelle
     * @param id
     */
    public void deleteStation(String id) { persistence.deleteStation(id); }

    /**
     * Sortiert die Tankstellen nach einer gegebenen Art
     */
    public void sortStations () {
        switch (mSort) {
            case "Super E5":
                Comparator<Station> comE5 = new Comparator<Station>() {
                    @Override
                    public int compare(Station lhs, Station rhs) {
                        return (lhs.getE5() < rhs.getE5()) ?  -1 :  1;
                    }
                };
                Collections.sort((List<Station>) this.mStations, comE5);
                break;

            case "Super E10":
                Comparator<Station> comE10 = new Comparator<Station>() {
                    @Override
                    public int compare(Station lhs, Station rhs) {
                        return (lhs.getE10() < rhs.getE10()) ?  -1 :  1;
                    }
                };
                Collections.sort((List<Station>) this.mStations, comE10);
                break;

            case "Diesel":
                Comparator<Station> comDiesel = new Comparator<Station>() {
                    @Override
                    public int compare(Station lhs, Station rhs) {
                        return (lhs.getDiesel() < rhs.getDiesel()) ?  -1 :  1;
                    }
                };
                Collections.sort((List<Station>) this.mStations, comDiesel);
                break;

            case "Entfernung":
                Comparator<Station> comDistance = new Comparator<Station>() {
                    @Override
                    public int compare(Station lhs, Station rhs) {
                        return (lhs.getDist() < rhs.getDist()) ?  -1 :  1;
                    }
                };
                Collections.sort((List<Station>) this.mStations, comDistance);
                break;
        }
    }

    public void setmRadius(int mRadius) {
        if (mRadius < 0) {
            this.mRadius = 1;
        } else if (mRadius > 25) {
            this.mRadius = 25;
        } else {
            this.mRadius = mRadius;
        }
    }

    public Collection<Station> getmStations() { return mStations; }

    public void setmSort(String mSort) { this.mSort = mSort; }

    public void setmSearchMethod(String mSearchMethod) { this.mSearchMethod = mSearchMethod; }

    public void setmZip(String mZip) { this.mZip = mZip; }

    public String getSort() { return mSort; }

    public Location getmLocation() { return mLocation; }

    public void setmLocation(Location mLocation) { this.mLocation = mLocation; }
}

