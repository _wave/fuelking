package de.andreerk.android.fuelking.core.utility;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Andre Erk on 12.06.2016.
 */
public abstract class RestAsyncTask  extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... params) {
        String response = "";

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(params[0]);
        Log.e("PARAMS", params[0]);
        try {
            HttpResponse execute = client.execute(httpGet);
            InputStream content = execute.getEntity().getContent();

            BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
            String s = "";
            while ((s = buffer.readLine()) != null) {
                response += s;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("RESPONSE", response);
        return response;
    }

    protected abstract void onPostExecute(String result);
}
