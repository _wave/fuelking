package de.andreerk.android.fuelking.gui;

import android.location.Location;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import de.andreerk.android.fuelking.R;
import de.andreerk.android.fuelking.core.StationManager;
import de.andreerk.android.fuelking.core.model.Station;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StationMapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StationMapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StationMapFragment extends Fragment {

    private StationMapFragment.OnFragmentInteractionListener mListener;
    private SupportMapFragment mSupportMapFragment;

    private StationManager mStationManager;
    private MapView mMapView;
    private GoogleMap googleMap;

    public static StationMapFragment newInstance() {
        StationMapFragment fragment = new StationMapFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_station_map, container, false);

        // StationManager-Instanz holen
        mStationManager = StationManager.getInstance(getActivity().getApplicationContext());

        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.mapView, mSupportMapFragment).commit();
        }

        if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    if (googleMap != null) {

                        googleMap.getUiSettings().setAllGesturesEnabled(true);
                        // Jede Tankstelle in Karte als Marker einfuegen
                        for (Station station: mStationManager.getmStations()) {
                            googleMap.addMarker(new MarkerOptions().position(new LatLng(station.getLat(), station.getLng())).title(station.getName()));
                        }
                        // Karte auf aktuelle Position zentrieren und zoomen
                        Location loc = mStationManager.getDeviceLocation();
                        LatLng pos = new LatLng(loc.getLatitude(), loc.getLongitude());
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(pos).zoom(13.0f).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        googleMap.moveCamera(cameraUpdate);
                    }
                }
            });
        }
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
//        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mMapView.onLowMemory();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StationMapFragment.OnFragmentInteractionListener) {
            mListener = (StationMapFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}