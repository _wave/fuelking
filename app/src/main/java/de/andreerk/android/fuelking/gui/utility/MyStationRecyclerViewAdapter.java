package de.andreerk.android.fuelking.gui.utility;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.andreerk.android.fuelking.R;
import de.andreerk.android.fuelking.core.model.Station;
import de.andreerk.android.fuelking.gui.StationListFragment.OnListFragmentInteractionListener;

public class MyStationRecyclerViewAdapter extends RecyclerView.Adapter<MyStationRecyclerViewAdapter.ViewHolder> {

    private List<Station> mStations;
    private final OnListFragmentInteractionListener mListener;
    private RecyclerViewClickListener mItemListener;


    public MyStationRecyclerViewAdapter(OnListFragmentInteractionListener listener, RecyclerViewClickListener itemListener) {
        mStations = new ArrayList<>();
        mListener = listener;
        mItemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.station_list_item, parent, false);
        return new ViewHolder(view);
    }

    private static String removeLastChar(String str) {
        return str.substring(0,str.length()-1);
    }

    /**
     * Entfernt den letzten Buchstaben von einem String
     * @param doubleVal
     * @return
     */
    private static String removeLastChar(Double doubleVal) {
        String str = String.valueOf(doubleVal);
        return str.substring(0,str.length()-1);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        try {
            holder.mStation = mStations.get(position);
            holder.mNameView.setText(mStations.get(position).getName());
            holder.mFuelView.setText("Super: "+removeLastChar(mStations.get(position).getE5())+" | E10: "+removeLastChar(mStations.get(position).getE10())+" | Diesel: "+removeLastChar(mStations.get(position).getDiesel()) );
            holder.mStreetView.setText(mStations.get(position).getStreet() + " " + mStations.get(position).getHouseNumber());
            holder.mPlaceView.setText(mStations.get(position).getPostCode() + " " + mStations.get(position).getPlace());
            holder.mDistanceView.setText(String.valueOf(mStations.get(position).getDist() + " km"));
            holder.mOpenView.setText(mStations.get(position).isOpen() ? "offen" : "zu");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mStations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mNameView;
        public final TextView mFuelView;
        public final TextView mStreetView;
        public final TextView mPlaceView;
        public final TextView mDistanceView;
        public final TextView mOpenView;
        public Station mStation;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.name);
            mFuelView = (TextView) view.findViewById(R.id.fuel);
            mStreetView = (TextView) view.findViewById(R.id.street);
            mPlaceView = (TextView) view.findViewById(R.id.place);
            mDistanceView = (TextView) view.findViewById(R.id.distance);
            mOpenView = (TextView) view.findViewById(R.id.open);

            view.setOnClickListener(this);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mFuelView.getText() + "'";
        }

        @Override
        public void onClick(View v) {
            mItemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }

    public List<Station> getmStations() {
        return mStations;
    }

    public void setmStations(Collection<Station> stations) {
        this.mStations = (List<Station>) stations;
        this.notifyDataSetChanged();
    }
}
