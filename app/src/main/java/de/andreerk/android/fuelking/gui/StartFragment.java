package de.andreerk.android.fuelking.gui;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import de.andreerk.android.fuelking.R;
import de.andreerk.android.fuelking.core.StationManager;

public class StartFragment extends Fragment {

    private SeekBar mSeekBar;
    private TextView mTvRadiusValue;
    private TextView mTvSorting;
    private Spinner mSpSorting;
    private CheckBox mCbRememberInput;
    private RadioButton mRbGps;
    private RadioButton mRbGpsZip;
    private RadioButton mRbZip;
    private EditText mEtZip;

    private StationManager mStationManager;

    private OnFragmentInteractionListener mListener;

    // 'Schluessel' fuer die Sharedreferences
    private String shpSave = "de.andreerk.android.fuelking.save";
    private String shpRadius = "de.andreerk.android.fuelking.radius";
    private String shpSearchMethod = "de.andreerk.android.fuelking.searchMethod";
    private String shpZip = "de.andreerk.android.fuelking.zip";
    private String shpSort = "de.andreerk.android.fuelking.sort";


    public StartFragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment StartFragment.
     */
    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FrameLayout ll = (FrameLayout)inflater.inflate(R.layout.fragment_start, container, false);

        // StationManager-Instanz holen
        this.mStationManager = StationManager.getInstance(getActivity().getApplicationContext());

        // Views holen
        mSeekBar = (SeekBar) ll.findViewById(R.id.sbRadius);
        mTvRadiusValue = (TextView) ll.findViewById(R.id.tvRadiusValue);
        mTvSorting = (TextView) ll.findViewById(R.id.tvSorting);
        mSpSorting = (Spinner) ll.findViewById(R.id.spSorting);
        mCbRememberInput = (CheckBox) ll.findViewById(R.id.cbRememberInput);
        mRbGps = (RadioButton) ll.findViewById(R.id.rbGPS);
        mRbZip = (RadioButton) ll.findViewById(R.id.rbZIP);
        mEtZip = (EditText) ll.findViewById(R.id.etZip);

        // Default Einstellungen
        mRbGps.setChecked(true);
        mStationManager.setmSearchMethod("GPS");

        // SharedPreferences laden und in StationManager setzen
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        if (sharedPreferences.getInt(shpSave, -1) == 1) {
            int radius = sharedPreferences.getInt(shpRadius, 12);
            mSeekBar.setProgress(radius);
            mStationManager.setmRadius(radius);

            String searchMethod = sharedPreferences.getString(shpSearchMethod, "GPS");
            switch (searchMethod) {
                case "GPS":
                    mRbGps.setChecked(true);
                    mStationManager.setmSearchMethod("GPS");
                    break;

                case "ZIP":
                    mRbZip.setChecked(true);
                    mStationManager.setmSearchMethod("ZIP");
                    String zip = sharedPreferences.getString(shpZip, "49086");
                    mEtZip.setText(zip);
                    mStationManager.setmZip(zip);
                    break;
            }

            int sort = sharedPreferences.getInt(shpSort, 0);
            mSpSorting.setSelection(sort);

            mCbRememberInput.setChecked(true);
        }

        // Bei Aenderung SharedPreferences speichern, wenn isChecked
        mCbRememberInput.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // SharedPreferences speichern
                    SharedPreferences shp = getActivity().getPreferences(Context.MODE_PRIVATE);
                    shp.edit().putInt(shpSave, 1).apply();
                    shp.edit().putInt(shpRadius, mSeekBar.getProgress()).apply();

                    if (mRbGps.isChecked()) {
                        shp.edit().putString(shpSearchMethod, "GPS").apply();
                    } else if (mRbZip.isChecked()) {
                        shp.edit().putString(shpSearchMethod, "ZIP").apply();
                        shp.edit().putString(shpZip, mEtZip.getText().toString()).apply();
                    }

                    shp.edit().putInt(shpSort, mSpSorting.getSelectedItemPosition()).apply();
                } else {
                    SharedPreferences shp = getActivity().getPreferences(Context.MODE_PRIVATE);
                    shp.edit().putInt(shpSave, 0).apply();
                }
            }
        });

        // Bei Aenderung Sortierung in StationManager setzen
        mSpSorting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getSelectedItem().toString();
                mStationManager.setmSort(selectedItem);
                mCbRememberInput.setChecked(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        // Bei Aenderung Radius in StationManager setzen
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private int progressVal;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressVal = progress;
                mTvRadiusValue.setText(progressVal + 1 + " km");
                mStationManager.setmRadius(progressVal + 1);
                mCbRememberInput.setChecked(false);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mTvRadiusValue.setText(progressVal + 1 + " km");
            }
        });

        return ll;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
