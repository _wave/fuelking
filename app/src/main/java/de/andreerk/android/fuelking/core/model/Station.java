package de.andreerk.android.fuelking.core.model;

import java.sql.Timestamp;

/**
 * Created by Andre Erk on 12.06.2016.
 * Normale Bean fuer eine Tankstelle
 */
public class Station {

    private int uid;
    private String name;
    private double lat;
    private double lng;
    private String brand;
    private double dist;
    private double diesel;
    private double e5;
    private double e10;
    private String id;
    private String street;
    private String houseNumber;
    private int postCode;
    private String place;
    private boolean isOpen;
    private Timestamp timestamp;
    private int hour;


    public Station() { }


    public int getUid() { return uid; }

    public void setUid(int uid) { this.uid = uid; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public double getDiesel() {
        return diesel;
    }

    public void setDiesel(double diesel) {
        this.diesel = diesel;
    }

    public double getE5() {
        return e5;
    }

    public void setE5(double e5) {
        this.e5 = e5;
    }

    public double getE10() {
        return e10;
    }

    public void setE10(double e10) {
        this.e10 = e10;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public Timestamp getTimestamp() { return timestamp; }

    public void setTimestamp(Timestamp timestamp) { this.timestamp = timestamp; }

    public int getHour() { return hour; }

    public void setHour(int hour) { this.hour = hour; }


    @Override
    public String toString() {
        return "Station{" +
                "brand='" + brand + '\'' +
                ", uid=" + uid +
                ", name='" + name + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", dist=" + dist +
                ", diesel=" + diesel +
                ", e5=" + e5 +
                ", e10=" + e10 +
                ", id='" + id + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", postCode=" + postCode +
                ", place='" + place + '\'' +
                ", isOpen=" + isOpen +
                ", timestamp=" + timestamp +
                ", hour=" + hour +
                '}';
    }
}
