package de.andreerk.android.fuelking.core.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Andre Erk on 24.06.2016.
 * Normale Bean um die Ergebnisse einer Datenbankabfrage strukturiert zurueckzugeben
 */
public class StatisticsEntry {

    private int hour;
    private double avgE10;
    private double avgE5;
    private double avgDiesel;


    public StatisticsEntry () {
    }

    public double getAvgDiesel() {
        return avgDiesel;
    }

    public void setAvgDiesel(double avgDiesel) {
        this.avgDiesel = avgDiesel;
    }

    public double getAvgE10() {
        return avgE10;
    }

    public void setAvgE10(double avgE10) {
        this.avgE10 = avgE10;
    }

    public double getAvgE5() {
        return avgE5;
    }

    public void setAvgE5(double avgE5) {
        this.avgE5 = avgE5;
    }

    public int getHour() { return hour; }

    public void setHour(int hour) { this.hour = hour; }

    @Override
    public String toString() {
        return "StatisticsEntry{" +
                "avgDiesel=" + avgDiesel +
                ", hour=" + hour +
                ", avgE10=" + avgE10 +
                ", avgE5=" + avgE5 +
                '}';
    }
}
